'use strict';

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const div = document.querySelector('#root');

books.forEach((el) => {
    if (!el.name) console.error(`Не вистачає властивості name`);
    else if (!el.author) console.error(`Не вистачає властивості author`);
    else if (!el.price) console.error(`Не вистачає властивості price`);

    else {
        const ul = document.createElement('ul');

    const liName = document.createElement('li');
    const liAuthor = document.createElement('li');
    const liPrice = document.createElement('li');

    liName.innerText = el.name;
    ul.append(liName);

    liAuthor.innerText = el.author;
    ul.append(liAuthor);

    liPrice.innerText = el.price;
    ul.append(liPrice);

    div.append(ul);
    }
})













//   let books = [
//     { author: "J.K. Rowling", name: "Harry Potter", price: 25.99 },
//     { author: "George Orwell", name: "1984", price: 19.99 },
//     // Додайте інші книги за потреби
// ];

// for (let book of books) {
//     if (!book.author || !book.name || !book.price) {
//         console.error("Помилка: не всі властивості присутні в об'єкті", book);
//     }
// }

// // Генерація списку на сторінці
// const rootElement = document.getElementById("root");
// const ul = document.createElement("ul");

// for (let book of books) {
//     const li = document.createElement("li");
//     li.textContent = `${book.name} (${book.author}) - $${book.price}`;
//     ul.appendChild(li);
// }

// rootElement.appendChild(ul);